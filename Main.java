package at.kara.football;

import java.util.Date;

public class Main {
	
	public static void main(String[] args) {
		Fieldplayer fp1 = new Fieldplayer("abou",  new Date(), 10000000);
		Goalkeeper gk1 = new Goalkeeper("fabricio", new Date(), , 5);
		
		Team t1 = new Team("Team1");
		t1.addPlayer(fp1);
		t1.addPlayer(gk1);
		
		System.out.println(t1.getValueOfTeam());
		t1.getPlayerList();

	}

}
