package at.kara.football;

import java.util.ArrayList;
import java.util.List;

public class Team {

	private List<Player> playerList = new ArrayList<>();
	private String name;
	
	public Team(String name) {
		super();
		this.name = name;
	}

	public int getValueOfTeam() {
		int TeamValue = 0;
		for (Player player : playerList) {
			TeamValue += player.getValue();
		}
		return TeamValue;
	}
	
	public void addPlayer(Player playerName) {
		playerList.add(playerName);
	}

	public List<Player> getPlayerList() {
		return playerList;
	}

	public String getName() {
		return name;
	}
	
}
