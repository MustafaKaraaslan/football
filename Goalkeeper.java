package at.kara.football;

import java.sql.Date;

public class Goalkeeper extends Player {
	
private int reactionTime;
	
	public Goalkeeper(String name, java.util.Date date, int value, int reactTime) {
		super(name, date, value);
		this.reactionTime = reactTime;
	}

	public int getReactionTime() {
		return reactionTime;
	}

}
