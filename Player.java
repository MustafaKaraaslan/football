package at.kara.football;

import java.sql.Date;


public class Player {
	
private String name;
private Date birthday;
private int value;

	
	public Player(String name, java.util.Date date, int value) {
		this.name = name;
		this.birthday = date;
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
